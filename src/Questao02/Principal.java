package Questao02;

import java.util.Scanner;

public class Principal {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        Elementos lista = new Elementos();
        int opc;

        do{
            System.out.println(" ");
            System.out.println("[1] INSERIR ELEMENTOS");
            System.out.println("[2] VERIFICAR SE ESTA EM ORDEM CRESCENTE");
            System.out.println("[3] LISTAR");
            System.out.println("[0] SAIR");
            System.out.print("OPCAO: ");
            opc = input.nextInt();

            switch (opc){
                case 1:
                    System.out.print("Informe o elemento: ");
                    int elemento = input.nextInt();
                    try {
                        lista.incluir(elemento);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("Esta ordenado? -> " + lista.estahOrdenada(lista) + " <-");
                    break;
                case 3:
                    System.out.println("--- LISTA ---");
                    System.out.println(lista);
                    break;
                default:
                    opc = 0;
                    break;
            }
        }while (opc != 0);
    }
}
